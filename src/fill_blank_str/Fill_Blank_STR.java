/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fill_blank_str;

import com.opencsv.CSVReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author analyst
 */
public class Fill_Blank_STR {

    private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_CONNECTION = "jdbc:mysql://localhost:3306/gsk_kki_db";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "";

    private static String the_string = "";
    private static String the_data = "";

    private static String medical_name = "";
    private static String city_name = "";
    private static String practice_place_name = "";
    private static String competence_name = "";

    private static int item_found = 0;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        // TODO code application logic here
        int the_result_checkToDB = 0;
        String csvFilename = "d:\\gsk\\project\\EXCEL FILES\\TUGAS HUDA_SLT 2016 - 2017.csv";

        try {
            CSVReader reader = new CSVReader(new FileReader(csvFilename), ',', '"', 0);
            String[] nextLine;
            int lineNumber = 0;
            // <editor-fold defaultstate="collapsed" desc=" read CSV FILE ">

            while ((nextLine = reader.readNext()) != null) {
                the_result_checkToDB = 0;
                lineNumber++;
                System.out.println("Line # " + lineNumber);

                // nextLine[] is an array of values from the line
                medical_name = nextLine[5].replace(".", medical_name).trim();
                city_name = nextLine[8].trim();
                practice_place_name = nextLine[6];
                //competence_name = nextLine[7].trim();

                System.out.println(medical_name + "----" + practice_place_name + "----" + nextLine[7].trim() + "----" + city_name);

                // <editor-fold defaultstate="collapsed" desc=" convert competence's veeva to kki's competence ">
                String veeva_competence = nextLine[7].trim();

                if (veeva_competence.matches(".*nternal .*")) 
                    competence_name = "penyakit dalam";
                 else if (veeva_competence.matches("Neurology")) 
                    competence_name = "Saraf";
                 else if (veeva_competence.matches("Pediatric")) 
                    competence_name = "Spesialis anak";
                 else if (veeva_competence.matches(".*Pulmonary.*")) 
                    competence_name = "Spesialis paru";
                 else if (veeva_competence.matches(".*Rehabilitation.*")) 
                    competence_name = "Kedokteran Fisik dan Rehabilitasi";
                 else if (veeva_competence.matches(".*Orthopedic.*")) 
                    competence_name = "Orthopaedi";
                 else if (veeva_competence.matches(".*Urology.*")) 
                    competence_name = "urologi";
                 else if (veeva_competence.matches(".*Thoracic.*")) 
                    competence_name = "toraks";
                 else if (veeva_competence.matches("General.*") || veeva_competence.matches("GP.*")
                         || veeva_competence.matches("Registered Doc.*") || veeva_competence.matches("Clinic Doctor")
                         || veeva_competence.matches("Consultant Physician") || veeva_competence.matches("Dispensing General Physician")
                         || veeva_competence.matches(".*Doctor in Training.*") 
                         ) 
                    competence_name = "dokter";
                 else if (veeva_competence.matches("Rehabilitation Medici.*")) 
                    competence_name = "rehabilitasi";
                 else if (veeva_competence.matches("Dentist.*")) 
                    competence_name = "gigi";
                 else if (veeva_competence.matches("Anaesthes.*")) 
                    competence_name = "Anestesi";
                 else if (veeva_competence.matches("Anatomic/Clinical Pathology")) 
                    competence_name = "Patologi Klinik";
                 else if (veeva_competence.matches("Anatomical Pathology")) 
                    competence_name = "Patologi Anatomi";
                 else if (veeva_competence.matches(".*Gynaecolog.*")) 
                    competence_name = "Kedokteran Fisik dan Rehabilitasi";
                 else if (veeva_competence.matches(".*Cardiovascular.*")) 
                    competence_name = "Jantung dan Pembuluh Darah";
                 else if (veeva_competence.matches(".*Psychiatry.*")) 
                    competence_name = "Kedokteran Jiwa";
                 else if (veeva_competence.matches(".*Derma.*") || veeva_competence.matches("Dermatology and venerology")) 
                    competence_name = "Kulit dan Kelamin";
                else if (veeva_competence.matches("E.N.T.")) 
                    competence_name = "Kulit dan Kelamin";
                

                //</editor-fold> convert competence's veeva to kki's competence
                if (nextLine[4].equalsIgnoreCase("")) {
                    //test 1 (standar/original parameter)
                    the_result_checkToDB = checkToDB(1, medical_name, competence_name, city_name);

                    //test 2 (split string into word)
                    String[] arr = medical_name.split(" ");
                    if (the_result_checkToDB == 0 && arr.length > 0) {

                        for (String ss : arr) {
                            the_result_checkToDB = checkToDB(2, "%" + ss + "%", competence_name, city_name);
                            if (the_result_checkToDB == 1) {
                                break;
                            }
                            //System.out.println(ss);
                        }
                    }

                    if (the_result_checkToDB == 0) {
                        the_string += (System.getProperty("line.separator"));
                    }
                } else {
                    the_string += (nextLine[4].trim());
                    the_string += (System.getProperty("line.separator"));
                    continue;
                }

                System.out.println("the_result_checkToDB : " + the_result_checkToDB);
                System.out.println("=====================================================");

                // <editor-fold defaultstate="collapsed" desc=" do empty string ">
                medical_name = "";
                city_name = "";
                practice_place_name = "";
                competence_name = "";
                //</editor-fold> do empty string
            }
            //</editor-fold> read CSV file
        } catch (Exception ex) {
            System.out.println(ex);
        }

        System.out.println("item found = " + item_found);

        String file_dir = "";
        file_dir = System.getProperty("user.home") + File.separatorChar + "My Documents";

        String the_timeStamp = new SimpleDateFormat("yyyy_MM_dd").format(Calendar.getInstance().getTime());

        String file_result_name = file_dir + "\\result_folder\\" + "Fill_Blank_STR_" + the_timeStamp + ".txt";
        writeToFile(the_string, file_result_name);
    }

    private static int checkToDB(int method_number, String name_param, String competence_param, String city_param) throws SQLException {
        int the_return = 0;
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;

        // <editor-fold defaultstate="collapsed" desc=" comparing to DB ">
        try {

            dbConnection = getDBConnection();
            String SQLCount = "select count(*) from 12_2017 where name like ? and competence like ? and city like ?";
            preparedStatement = dbConnection.prepareStatement(SQLCount);
            preparedStatement.setString(1, name_param);

            if (competence_param.matches("dokter")) {
                preparedStatement.setString(2, competence_param);
            } else {
                preparedStatement.setString(2, "%" + competence_param + "%");
            }

            preparedStatement.setString(3, "%" + city_param + "%");

            System.out.println("After : " + preparedStatement.toString());

            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                int numberOfRows = rs.getInt(1);

                // <editor-fold defaultstate="collapsed" desc=" get 1 row ">
                if (numberOfRows == 1) { // 

                    String query = "SELECT * FROM 12_2017 WHERE name like ? and competence like ? and city like ?";
                    try {
                        dbConnection = getDBConnection();
                        preparedStatement = dbConnection.prepareStatement(query);
                        preparedStatement.setString(1, name_param);

                        if (competence_param.matches("dokter")) {
                            preparedStatement.setString(2, competence_param);
                        } else {
                            preparedStatement.setString(2, "%" + competence_param + "%");
                        }

                        preparedStatement.setString(3, "%" + city_param + "%");

                        // execute select SQL stetement
                        ResultSet rs_selectSQL = preparedStatement.executeQuery();

                        while (rs_selectSQL.next()) {

                            String the_str_number = rs_selectSQL.getString("STR_NUMBER");
                            String the_name = rs_selectSQL.getString("NAME");
                            String the_competence = rs_selectSQL.getString("COMPETENCE");
                            String the_city = rs_selectSQL.getString("CITY");
                            String the_practice_place = rs_selectSQL.getString("PRACTICE_PLACE");

                            if (method_number == 2) {
                                the_string += (the_str_number.trim() + ",-bbbbbbbbb");
                            } else if (method_number == 1) {
                                the_string += (the_str_number.trim() + ",-aaaaaaaaa");
                            }
                            the_string += (System.getProperty("line.separator"));

                            System.out.println("=== Found 1 data. name_param : " + medical_name + " ===");
                            System.out.println("STR : " + the_str_number);
                            System.out.println("NAME : " + the_name);
                            System.out.println("PRCTC PLACE : " + the_practice_place);
                            System.out.println("COMPETENCE : " + the_competence);
                            System.out.println("CITY : " + the_city);

                            break;
                        }

                    } catch (SQLException e) {

                        System.out.println(e.getMessage());

                    }
                    item_found++;
                    the_return = 1;

                } //</editor-fold>
                else {
                    the_return = 0;
                }
//                else {
//                    the_string += (System.getProperty("line.separator"));
//                }

            } else {
                System.out.println("error: could not get the record counts");
            }
        } catch (SQLException e) {

            System.out.println(e.getMessage());

        }

        if (preparedStatement != null) {
            preparedStatement.close();
        }

        if (dbConnection != null) {
            dbConnection.close();
        }

        //</editor-fold>
        return the_return;
    }

    private static Connection getDBConnection() {

        Connection dbConnection = null;

        try {

            Class.forName(DB_DRIVER);

        } catch (ClassNotFoundException e) {

            System.out.println(e.getMessage());

        }

        try {

            dbConnection = DriverManager.getConnection(
                    DB_CONNECTION, DB_USER, DB_PASSWORD);
            return dbConnection;

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        }

        return dbConnection;

    }

    private static void writeToFile(String the_string_param, String filename_result) {

        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            File file = new File(filename_result);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            // true = append file
            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);

            bw.write(the_string_param);

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null) {
                    bw.close();
                }

                if (fw != null) {
                    fw.close();
                }

            } catch (IOException ex) {

                ex.printStackTrace();

            }
        }
    }

}
